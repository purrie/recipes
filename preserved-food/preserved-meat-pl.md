---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Zapieczone Mięso}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](preserved-meat.jpg){width=40%}


# Składniki
- Mięso (dowolna ilość)
- Sól
- Ziele Anglielskie
- Liść Laurowy

\End{multicols}

# Przygotowanie
Mięso należy pokroić na drobno, posolić i wymieszać by dobrze rozprowadzić sól w mięsie. Mięso zostawić na godzine by przeszło solą. 

Potem włożyć mięso do słoików, tak by zapełnić około dwie trzecie pojemności słoika, dodać dwa ziarka ziela angielskiego i jeden liść laurowy. Słoiki zamknąć i wstawić do piekarnika nastawionego na 120C na około 3 godziny. Mięso należy doglądać co jakiś czas i gdy przy końcu czasu pieczenia wyjdzie na wieszk woda, to można wyłączyć pieczenie wcześniej, lub nastawić na dłużej dopuki tak się nie stanie. 

Po wyłączeniu piekarnika, należy zostawić słoiki w piekarniku aż do ostudzenia.

# Notatki
Do mięsa można dodać też inne przyprawy poza solą.

Mięso można jeść zaraz po ostudzeniu ale można je także przechowywać przez długi czas, latami nawet, jeśli przetrzymuje się je w niskiej temperaturze, na przykład w piwnicy.
