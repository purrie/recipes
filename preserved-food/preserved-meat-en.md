---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Preserved Meat}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](preserved-meat.jpg){width=40%}


# Ingredients
- Meat (any amount)
- Salt
- Allspice
- Bay Leaf

\End{multicols}

# Preparation
Cut the meat into small pieces. Salt it and mix it to spread the salt evenly. Leave the meat for about an hour to let it absorb the salt. 

Fill jars with the meat to about two thirds of jar's capacity, add two grains of allspice and one bay leaf. Seal the jars and put them into the oven. Bake in 120C (248F) temperature for about 3 hours.

You should keep checking up on the meat every so often, and when you notice water gathering on the top layer of the meat towards the end of the time of baking, you can turn the oven off early, otherwise you should wait until you notice the water, baking the meat longer if need be. After you turn off the oven, don't remove the jars from it, let them naturally and slowly cool down inside.

# Notes
You can add other spices to the meat besides salt too.

The meat is ready to eat after cooling off but it can be stored in relatively cold temperatures (in a basement for example) for a long time, a few years even.
