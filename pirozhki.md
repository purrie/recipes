# Пирожки (Pirozhki)

Russian stuffed buns

Makes around 40

## Dough

* 1/2 cup flour !this is wrong. I wonder if it's supposed to be 5 cups.
* 1 teaspoon salt
* 1 cup butter
* 1/2 cup lard
* 1/2 to 3/4 cup of cold water

1. Combine flour, salt, butter and lard in a large bowl.
2. Rub fat and flour together with fingers until it looks like coarse meal.
3. Add 1/4 cup water and gather into a ball.
4. If it crumbles, add another tablespoon, repeating until all adheres.
5. Wrap in waxed paper and chill for an hour.
6. On floured surface roll out and fold 3-4 times.
7. Re-wrap in wax paper and chill for an hour

## Filling

* 1/4 cup butter
* 3 cups of finely chopped onions.
* 1-1/2 pounds of ground beef
* 3 finely chopped hard-boiled eggs
* 6 tablespoons finely cut dill leaves ! this looks suspiciously large
* 2 tablespoons salt ! this looks suspiciously large
* 1/2 teaspoon black pepper

1. Cook onions in butter until transparent.
2. Add hamburger and cook completely.
3. Chop finely when cooked.
4. Add eggs, dill, salt, and pepper.

5. Heat oven to 400F.
6. Roll dough into 1/8" thick circle.
7. Cut out as many 3" to 3-1/2" circles as possible.
8. Re-roll dough and repeat.
9. Drop 2 tablespoons of filling into the centre of each circle.
10. Fold each.
    1. Fold top to middle.
    2. Fold sides in
    3. Fold bottom up.
11. Place seam down on buttered baking sheet.
12. Bake for 30 minutes until brown.
