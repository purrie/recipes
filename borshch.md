# Борщ (Borshch)

* 3 pounds of beef short ribs
* 1 gallon water
* 2 large carrots
* 1 gallon chopped cabbage
* 1 large onion (chopped)
* 1 cup chopped celery
* 3 cups ketchup
* 5 quarts chopped beets with juice

1. Simmer (don't boil) beef in water and skim water.
2. Add carrots, cabbage, onion, celery, ketchup.
3. Simmer all together until the meat is tender.
4. Remove meat, discard fat and bones.
5. Chop meat and return to soup.
6. Add beets, salt and pepper.
7. Simmer until done.

Serve with sour cream.
