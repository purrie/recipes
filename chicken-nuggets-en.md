---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Chicken Nuggets}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](chicken-nuggets.jpg){width=40%}

# Ingredients
- 0.5kg Chicken breasts
- 1 Egg
- Breading
- Spices to your taste


# Preparation
Mix the egg with spices inside a bowl. Wash chicken breasts and cut them into small chunks, put them into the bowl with the egg and mix well to coat the meat. Make sure to cut off any bad parts or bones of the meat. Leave in the mixture for about an hour. 

Prepare a plate on which you will coat the meat with breading. Pour cooking oil into a large pan, enough to let the meat be at least half submerged in it but no more. Put the breaded meat on warmed up oil and fry on medium high flame. Make sure to flip the meat regularly to not burn it. When meat is ready, transfer it onto a plate or a pot and continue until all is fried.

\End{multicols}

# Notes
You can cut the meat into whatever shapes or sizes you want, but avoid having pieces too thick as it's hard to fry them and you may end up having insides raw.

You can prepare more ingredients to make more nuggets using the same proportions.

Keep in mind that bits of different sizes will need different time frying so flip more often and take smaller bits off the pan sooner than larger.

You can test if the oil is warm enough by sprinkling a little of breading onto it. If it's sizzling then it's ready.

Take care when frying the chicken as it is easy to under cook or burn it. One way to help test it is to see if it is easy to pierce it with a fork, or a chopstick in case when you don't want to scratch your pan.

Oil will evaporate during frying, make sure to refill it to desired level as the meat will burn more easily without enough oil.

You can mix potato flour into the breading, that will create a crispy coating around the meat.
