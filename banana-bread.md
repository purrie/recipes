# Banana Bread

* 1/2 cup butter
* 1 cup sugar
* 2 eggs
* 2 mashed ripe bananas
* 2 cups flour
* 1 teaspoon baking soda
* Chopped nuts.

Pour into a greased loaf pan.

Bake 350F for 45-60 minutes.
