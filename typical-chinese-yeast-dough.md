# Typical Chinese Yeast Dough

Chinese steamed buns are made form this basic dough.

* 3 1/2 cups flour
* 4 tablespoons lard
* 1 teaspoon active dry yeast
* 1 1/3 cups of warm water
* 3 1/4 tablespoons sugar
* 1/2 teaspoon baking powder (optional)

1. Combine 3 cups of flour and lard in a large mixing bowl.
   Mix well with clean hands.
2. In a separate bowl, combine yeast, sugar, 1/2 cup warm water and remaining flour.
   Mix well.
3. Add yeast mixture to the flour-lard mixture.
   Blend well and slowly add the remaining 1/2 cup water.
   Knead for a few minutes until smooth.
4. Cover bowl with a damp towel and let rise in warm area (around 80F) for 1 3/4 to 2 hours or until doubled in bulk.
5. Punch center of the risen dough; transfer to floured surface and sprinkle with baking powder.
   Knead for a few minutes until smooth.
   If necessary, sprinkle extra flour while kneading.
6. Use dough in steamed bun recipes.

## Notes

Note 1: This original recipe calls for 4 teaspoons of lard but this doesn't seem to work.
Try it, and if that doesn't work, use a larger amount.

Note 2: The original recipe calls for 1 cup of warm water.
