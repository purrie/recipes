---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Ogórki w Musztardzie}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---
\Begin{multicols}{2}

![](pickled-cucumbers-mustard-flavor.jpg){width=40%}

\columnbreak

# Składniki
- Ogórki 
- Ziele Angielskie
- Liść Laurowy

# Zalewa
- 12 Łyżek musztardy
- 8 Szklanek Wody
- 2 Szklanki Octu
- 3 Szklanki Cukru
- 6 Łyżeczek Soli

\End{multicols}

# Przygotowanie

Ogórki należy poćwiartkować i umieścić w słoikach, do każdego słoika należy dodać dwa ziarka ziela angielskiego i jeden liść. 

Zalewe należy wymieszać w garnku na ciepło by lepiej rozpuścić cukier ale nie gotować. Po wymieszaniu zalać ogórki, zamknąć i zapasteryzować gotując w sporym garnku, zalanym do połowy słoików. Ogórki powinny zostać w gotującej się wodzie przez około 5 minut. Po zapasteryzowaniu słoiki odstawić do ostygnięcia, a następnie przenieść w zimne miejsce. Ogórki muszą się ostać przez co najmniej 3 dni zanim będą gotowe do sporzycia.

# Notatki

Żeby ogórki były soczyste to po podebraniu trzeba zostawić je w cieniu w zimnej wodzie na dwie godziny.

Trzeba zwrócić uwagę na to czy słoiki dobrze się zamkną przy pasteryzacji, w razie potrzeby wymienić zakrątke.


