---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Pickled Cucumbers - Paprika flavor}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---
\Begin{multicols}{2}

![](pickled-cucumbers-paprika-flavor.jpg){width=40%}

\columnbreak

# Ingredients
- Cucumbers (larger ones need to be quartered)
- Allspice
- Mustard seeds

# Solution
- 3 cups of water
- 1 cup of vinegar
- 1 cups of sugar
- 2 table spoons of salt
- 2 tea spoons of paprika

\End{multicols}

# Preparation

Cucumbers should be put in the jars, tightly packed. Add to each jar two grains of allspice and a tea spoon of mustard seeds. You can add strips of live pepper instead of adding the spice to the solution.

Solution should be mixed in a pot, cooking it but not boiling until all sugar disolves. When done, fill up the jars and pasteurize them in a big pot with water filled to about half the height of the jars. The jars should be kept in boiling water for about 10 to 15 minutes. After that, set the jars aside until they cool and move them into a cold place. The cucumbers are ready to eat after at least 3 days.

# Notes

Before use, it is best to soak the cucumbers in cold water in shade for at least 2 hours, this will make them more juicy.

Pay attention as to whatever the jars close properly while pasteurizing and replace the lids and repasteuirze if necessary.

You can use various kinds of paprika or peppers, for example, chilli peppers.

