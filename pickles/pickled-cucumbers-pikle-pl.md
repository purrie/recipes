---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Pikle}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---

\Begin{multicols}{2}

![](pickled-cucumbers-pikle.jpg){width=40%}

# Składniki
- Ogórki 
- Koper
- Czosnek
- Chrzan
- Ziele Angielskie
- Gorczyca
- Pieprz
- Cebula (Opcionalne)

# Zalewa
- 5 Szklanek wody
- 2 Szlanki octu
- 6 Łyżeczek cukru
- 5 Łyżek miodu
- 3 Łyżki soli

\End{multicols}

# Przygotowanie

Ogórki obrać, poćwiartować i wydrążyć nasiona. Posolić i zostawić na 24 godziny.

Do zrobienia zalewy należy zacząć od rozpuszczenia cukru i miodu w ciepłej gotującej się wodzie ale nie gotować. Potem dodać reszte składników i dobrze wymieszać.

Ogórki można skroić do wielkości kęsa, zwłąszcza by zmieścić je w mniejszych słoikach. Do słoików dodać koper, ząbek czosnku, trochę świeżego chrzanu, 2 kulki ziela angielskiego, łyżeczke gorczycy, trochę pieprzu i kilka pasków cebuli. Zalać zalewą i zapasteryzować w wodzie nie wyższej niż sięgającej pół słoika przez nie dłużej niż 5 minut.

# Notatki

Nie ma "właściwych" proporcji jeśli chodzi o składniki, należy je dodać zależnie od uznania. Zalewe należy zrobić tyle ile potrzeba do zalania wszystkich ogórków w podanych proporcjach.

Żeby ogórki były soczyste to po podebraniu trzeba zostawić je w cieniu w zimnej wodzie na dwie godziny.

Najlepiej użyć soli kamiennej.


