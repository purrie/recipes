---
geometry:
- top=20mm
- bottom=10mm
- right=20mm
- left=20mm
fontsize: 12pt
header-includes:
- \usepackage{fancyhdr}
- \pagenumbering{gobble}
- \pagestyle{fancy}
- \rhead{}
- \chead{}
- \lhead{\huge Pickled Cucumbers - Mustard flavor}
- \usepackage{multicol}
- \newcommand{\hideFromPandoc}[1]{#1}
- \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
    }
---
\Begin{multicols}{2}

![](pickled-cucumbers-mustard-flavor.jpg){width=40%}

\columnbreak

# Ingredients
- Cucumbers 
- Allspice
- Bay Leaf

# Solution
- 12 Table spoons of mustard
- 8 Cups of water
- 2 Cups of vinegar
- 3 Cups of sugar 
- 6 Tea spoon of salt

\End{multicols}

# Preparation

Cucumbers should be quartered and put in the jars. Add a bay leaf and two grains of allspice to each as well.

Solution should be mixed in a pot, cooking it but not boiling until all sugar disolves. When done, fill up the jars and pasteurize them in a big pot with water filled to about half the height of the jars. The jars should be kept in boiling water for about 5 minutes. After that, set the jars aside until they cool and move them into a cold place. The cucumbers are ready to eat after at least 3 days.

# Notes

Before use, it is best to soak the cucumbers in cold water in shade for at least 2 hours, this will make them more juicy.

Pay attention as to whatever the jars close properly while pasteurizing and replace the lids and repasteuirze if necessary.


