# Медивнук (Medivnuk)

This should be prepared 1-2 days before serving.

* 3/4 cup honey
* 1 teaspoon cinnamon
* 1/4 teaspoon cloves
* 1/4 teaspoon nutmeg
* 1 1/2 teaspoon baking soda
* 1 1/2 teaspoon baking powder
* 1/4 cup unsalted butter, softened
* 1/2 cup dark brown sugar
* 3 eggs, separated into whites and yolks
* 2 cups flour
* 1/2 teaspoon salt
* 1/2 cup raisins
* 1/4 cup dried currants
* 1/2 cup fine-chopped walnuts

1. Bring honey to a boil, stirring constantly in a 6-8 quart pan.
2. Add cinnamon, cloves, nutmeg, baking soda and let it cool.
3. In a large bowl, cream butter and brown sugar together.
4. Beat in egg yolks.
5. Add spiced honey.
6. Combine 1 1/2 cups of flour with salt and baking powder.
7. Add to sugar-egg mix 3-4 tablespoons at a time.
8. Add raisins, currants, walnuts.
9. Add the rest of the flour, folding.
10. Beat egg whites until it forms stiff peaks.
11. Fold whites into the batter.
12. Coat brown paper with butter and line the pan with buttery paper.
13. Put batter in pan(s).
14. Bake for 1 1/2 hours at 300F.
15. Peel off paper and let cool.
16. Put in waxed paper and let sit for 1-2 days.
